import { Icon, Img, Node, Rect, makeScene2D } from "@motion-canvas/2d";
import { createRef, all, delay, waitUntil, waitFor } from "@motion-canvas/core";
import unstyled from "@/assets/images/world-wide-web.png";
import styled from "@/assets/images/world-wide-web-styled.png";

export default makeScene2D(function* (view) {
  const imageRef1 = createRef<Img>();
  const imageRef2 = createRef<Img>();
  const maskRef1 = createRef<Rect>();
  const maskRef2 = createRef<Rect>();
  const iconRef1 = createRef<Icon>();
  const iconRef2 = createRef<Icon>();

  view.add(
    <Node cache>
      <Rect ref={maskRef1} fill={"pink"} x={0} y={0} />
      <Img
        ref={imageRef1}
        src={unstyled}
        radius={20}
        compositeOperation={"source-in"}
      />
    </Node>
  );

  view.add(
    <Node cache>
      <Rect ref={maskRef2} fill={"pink"} x={0} y={0} />
      <Img
        ref={imageRef2}
        src={styled}
        radius={20}
        compositeOperation={"source-in"}
      />
    </Node>
  );

  view.add(
    <Node ref={iconRef1} rotation={10} x={-imageRef1().width() / 2 - 50}>
      <Icon icon={"devicon:html5"} height={100} />
      <Icon icon={"noto-v1:hammer"} height={75} rotation={-20} x={-60} />
    </Node>
  );

  view.add(
    <Node ref={iconRef2} rotation={-10} y={-imageRef2().height() / 2 - 50}>
      <Icon icon={"devicon:css3"} height={100} />
      <Icon icon={"noto-v1:artist-palette"} height={75} rotation={10} x={60} />
    </Node>
  );

  maskRef1()
    .height(imageRef1().height())
    .width(imageRef1().width() + 100)
    .x(-maskRef1().width());
  maskRef2()
    .height(imageRef2().height() + 100)
    .width(imageRef2().width())
    .y(-maskRef2().height());

  yield* waitFor(1);
  yield* all(
    maskRef1().x(50, 2),
    iconRef1().x(imageRef1().width() / 2 + 100, 2)
  );
  yield* waitFor(2);
  yield* all(
    maskRef2().y(60, 2),
    iconRef2().y(imageRef2().height() / 2 + 60, 2)
  );
  yield* waitFor(2);
});

import { Icon, Img, Node, Rect, makeScene2D } from "@motion-canvas/2d";
import { createRef, all, delay, waitUntil, waitFor } from "@motion-canvas/core";
import unstyled from "@/assets/images/world-wide-web.png";
import styled from "@/assets/images/world-wide-web-styled.png";
import { Menu } from "@headlessui/react";

export default makeScene2D(function* (view) {
  const mouseRef = createRef<Img>();

  view.add(
    <Menu>
      <Menu.Button>More</Menu.Button>
      <Menu.Items>
        <Menu.Item>
          {({ active }) => (
            <a
              className={`${active && "bg-blue-500"}`}
              href="/account-settings"
            >
              Account settings
            </a>
          )}
        </Menu.Item>
        <Menu.Item>
          {({ active }) => (
            <a
              className={`${active && "bg-blue-500"}`}
              href="/account-settings"
            >
              Documentation
            </a>
          )}
        </Menu.Item>
        <Menu.Item disabled>
          <span className="opacity-75">Invite a friend (coming soon!)</span>
        </Menu.Item>
      </Menu.Items>
    </Menu>
  );
});

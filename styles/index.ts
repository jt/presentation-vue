import "./slidev.css"; // For modifications of Slidev builtin classes
import "./modifiers.css"; // For modifications of elements impacted by Slidev builtin classes
import "./standalone.css"; // For modifications of elements with no links with Slidev

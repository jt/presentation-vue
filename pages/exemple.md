---
layout: center
class: text-center
title: "Exemple : snoBoard"
---

<h1 v-click-hide class="example-title transition-all duration-1500">
Un petit exemple ?
</h1>

<h2 relative v-after class="example-snoboard !text-5xl font-bold text-primary transition-all duration-1500">
snoBoard<span class="blur-lg" absolute left-0 top-0>snoBoard</span>
</h2>

<img v-after src="/4-snoboard-table.png" alt="SnoBoard vue 7" height="764" width="1490" class="duration-700 delay-1000 scale-30 origin-top-left absolute top-8 left-20"/>
<img v-after src="/4-snoboard-interaction.png" alt="SnoBoard vue 3" height="677" width="1574" class="duration-700 delay-1200 scale-30 origin-top-left absolute top-51 left-5"/>
<img v-after src="/4-snoboard-struct.png" alt="SnoBoard vue 6" height="755" width="1600" class="duration-700 delay-1400 scale-30 origin-top-left absolute top-90 left-8"/>
<img v-after src="/4-snoboard-guide.png" alt="SnoBoard vue 2" height="550" width="1172" class="duration-700 delay-1600 scale-30 origin-top-left absolute top-95 left-86"/>
<img v-after src="/4-snoboard-chromosome.png" alt="SnoBoard vue 1" height="874" width="1600" class="duration-700 delay-1800 scale-30 origin-top-left absolute top-90 left-163"/>
<img v-after src="/4-snoboard-karyotype.png" alt="SnoBoard vue 4" height="535" width="1600" class="duration-700 delay-2000 scale-30 origin-top-left absolute top-55 left-165"/>
<img v-after src="/4-snoboard-sequence.png" alt="SnoBoard vue 5" height="942" width="1600" class="duration-700 delay-2200 scale-30 origin-top-left absolute top-5 left-155"/>

<!-- 
Et donc pour **conclure**, évidemment je l'**utilise**, dans le cadre du **développement** sur la **plateforme bioinfo** d'une **base de connaissance** sur les **modifications de l'ARN ribosomique par les petits ARN nucléolaires** qui s'appellera **snoBoard**, et les pouvoirs magiques de Vue permettront d'y  **représenter des structures secondaires**, ou encore de faire des **analyse comparatives**. -->

<!--
Évidemment, si je vous en parle, c'est que **j'utilise Vue**, en l'occurrence dans le cadre du **développement** sur la **plateforme bioinfo** d'une **base de connaissance** sur les **modifications de l'ARN** qui s'appellera :

**snoBoard** \*tadaa\*, et les pouvoirs magiques de Vue permettront d'y  **représenter des structures secondaires**, ou encore de faire des **analyse comparatives**.
-->

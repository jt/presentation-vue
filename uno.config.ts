import { defineConfig } from "unocss";

export default defineConfig({
  shortcuts: {
    "bg-primary": "bg-[#42b883]",
    "text-primary": "text-[#42b883]",
    "stroke-primary": "stroke-[#42b883]",
  },
});

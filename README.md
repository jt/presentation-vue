# Présentation Vue en 180 secondes

Ce dépôt contient les sources pour construire le diaporama de la présentation de
[Vue.js](http://vuejs.org) en 180 secondes.

Pour lancer le diaporama localement, exécuter:

- `npm install`
- `npm run dev`
- Se rendre sur : http://localhost:3030

Fait avec [Slidev](https://sli.dev/).

# Plan

## Web

- HTML / CSS
- JS
- Libs
- Frameworks -> Vue
- Quid des CMS ?

> Diapo :
> Timeline qui va de slide en slide vers la droite, avec les items successifs :
>
> - HTML/CSS : un code html (balises) -> se transforme en page web (la première du web par exemple http://info.cern.ch/hypertext/WWW/TheProject.html) dans un plop, puis un pinceau passe dessus et la style
> - Js : un dropdown, la souris clique et ça se déroule, c'est plusieurs couleurs, quand il clique ça change de couleur
> - Libs
> - Frameworks : Vue avec une cape de super-héros ?

## Vue

- Avantages (rapidité (setup en -1min), légèreté, etc... cf. pres équipe)
- Caractère progressif / évolutif (on peut rajouter au fur et à mesure ("quand vous y aurez trempé les lèvres, vous pourrez facilement boire une gorgée"))
- Aspect open source / indép des grandes entreprises
- Grands principes (composants, réactivité, déclaratif, SFC)

## Autour de Vue / fait avec

- Doc / SSG : Vitepress
- Slidev (onélà)
- Nuxt pour le back
- Headless CMS (Storyblok et + (chercher))
- Bibs de composants (pour design easy)
- Markdown ?

## Exemple

- snoBoard
